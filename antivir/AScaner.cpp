#include "stdafx.h"
#include "AScaner.h"


AScaner::AScaner()
{
}


AScaner::~AScaner()
{
}

zRPCAnswer AScaner::Scan(LPTSTR path)
{
	DWORD hScan = 0;
	DWORD res;
	zRPCAnswer answer;
	wchar_t szFileName[MAX_PATH];

	wcscpy_s(szFileName, MAX_PATH, path);

	hScan = StartScan(szFileName);

	while (1)
	{
		res = GetScanData(hScan, answer);

		if (res == ZSDK_REQUEST_OK){
			//printf("All scanned");
			break;
		}
		else if (res == ZSDK_REQUEST_MORE_DATA){
			//printf("More data");
		}
		else if (res == ZSDK_REQUEST_ERROR){
			answer.ScanStatus = -1;
			break;
		}
	}
	return answer;
}

// antivirDlg.h : ���� ���������
//

#pragma once
#include "afxwin.h"
#include "afxeditbrowsectrl.h"
#include "zsdk_def.h"

using namespace std;
// ���������� ���� CantivirDlg
class CantivirDlg : public CDialogEx
{
// ��������
public:
	CantivirDlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_ANTIVIR_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

private:
	static CStatic current_file;
	static CListBox errorList;
	static CMFCEditBrowseCtrl fbrowser;
	afx_msg void OnBnClickedButtonScan();

	static CButton b_scan;
	static bool isScanning;
	static CRITICAL_SECTION c2;
	static CString path;
	static int numCPU;
	static bool *s;//semafor
	static DWORD *id;//thread id
	static LPTSTR *file;//local memmory for threads
	static HANDLE hFind;

	static void scanFolder();
	static void task(LPVOID);
	static void GetFileList(LPTSTR path);
	static void addTask(LPTSTR p);

	
};

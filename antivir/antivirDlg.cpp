
// antivirDlg.cpp : ���� ����������
//

#include "stdafx.h"
#include "antivir.h"
#include "antivirDlg.h"
#include "afxdialogex.h"
#include "AScaner.h";
#include "zsdk_def.h";
#include <tchar.h> 
#include <stdio.h>
#include <strsafe.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CString CantivirDlg::path;
HANDLE CantivirDlg::hFind = INVALID_HANDLE_VALUE;
bool* CantivirDlg::s;//semafor*
DWORD *CantivirDlg::id;//thread id
LPTSTR *CantivirDlg::file;//local memmory for threads
bool CantivirDlg::isScanning = false;
int CantivirDlg::numCPU = 0;
CButton CantivirDlg::b_scan;//button scan
CListBox CantivirDlg::errorList;
CStatic CantivirDlg::current_file;
CMFCEditBrowseCtrl CantivirDlg::fbrowser;
CRITICAL_SECTION CantivirDlg::c2;//semafor**

CantivirDlg::CantivirDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CantivirDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CantivirDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_cur_file, current_file);
	DDX_Control(pDX, IDC_MFCEDITBROWSE1, fbrowser);
	DDX_Control(pDX, IDC_BUTTON_SCAN, b_scan);
	DDX_Control(pDX, IDC_LIST1, errorList);
}

BEGIN_MESSAGE_MAP(CantivirDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_SCAN, &CantivirDlg::OnBnClickedButtonScan)
END_MESSAGE_MAP()


// ����������� ��������� CantivirDlg

BOOL CantivirDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// ������ ������ ��� ����� ����������� ����.  ����� ������ ��� �������������,
	//  ���� ������� ���� ���������� �� �������� ����������
	SetIcon(m_hIcon, TRUE);			// ������� ������
	SetIcon(m_hIcon, FALSE);		// ������ ������

	// TODO: �������� �������������� �������������
	SYSTEM_INFO sysinfo;
	GetSystemInfo(&sysinfo);

	numCPU = sysinfo.dwNumberOfProcessors;
	InitializeCriticalSection(&c2);
	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

// ��� ���������� ������ ����������� � ���������� ���� ����� ��������������� ����������� ���� �����,
//  ����� ���������� ������.  ��� ���������� MFC, ������������ ������ ���������� ��� �������������,
//  ��� ������������� ����������� ������� ��������.

void CantivirDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // �������� ���������� ��� ���������

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// ������������ ������ �� ������ ����������� ��������������
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// ��������� ������
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// ������� �������� ��� ������� ��� ��������� ����������� ������� ��� �����������
//  ���������� ����.
HCURSOR CantivirDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CantivirDlg::OnBnClickedButtonScan()
{
	if (isScanning)// stoping the scan
	{
		isScanning = false;
		b_scan.SetWindowTextW(L"SCAN");
		fbrowser.SetReadOnly(false);
		return;
	}

	b_scan.SetWindowTextW(L"STOP");
	fbrowser.GetWindowTextW(path);
	fbrowser.SetReadOnly(true);
	HANDLE hmain = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)scanFolder, NULL,0, NULL);

}

void CantivirDlg::scanFolder()
{
	isScanning = true;

	id = new DWORD[numCPU];
	HANDLE *thread = new HANDLE[numCPU];
	s = new bool[numCPU];
	file = new LPTSTR[numCPU];
	for (size_t i = 0; i < numCPU; i++)
	{
		file[i] = NULL;
	}

	for (size_t i = 0; i < numCPU; i++)
		s[i] = true;//free semafor


	for (int i = 0; i < numCPU; i++)
	{
		thread[i] = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)task, (LPVOID)i, 0, &id[i]);
	}

	GetFileList(path.AllocSysString());
	isScanning = false;

	WaitForMultipleObjects(numCPU, thread, true, INFINITE);


	b_scan.SetWindowTextW(L"SCAN");
	fbrowser.SetReadOnly(false);
	delete id;
	delete thread;
	delete file;
}

void CantivirDlg::task(LPVOID p)
{
	AScaner scan;
	zRPCAnswer answer;
	int id = (int)p;
	while (isScanning)
	{
		if (file[id] == NULL)
				continue;
		if (!s[id])
		{
			answer = scan.Scan(file[id]);

			if (answer.ScanStatus = -1)
				current_file.SetWindowTextW(L"ZSDK_REQUEST_ERROR");

			s[id] = true;//free
			if (answer.VirCount > 0 && answer.VirCount<10000)//if zsdk request error
			{
				EnterCriticalSection(&c2);
					errorList.AddString(file[id]);
				LeaveCriticalSection(&c2);
			}
		}
	}
}

void CantivirDlg::GetFileList(LPTSTR path)
{
	WIN32_FIND_DATA ffd;
	TCHAR szDir[MAX_PATH];
	HANDLE hFind = INVALID_HANDLE_VALUE;

	StringCchCopy(szDir, MAX_PATH, path);
	StringCchCat(szDir, MAX_PATH, TEXT("\\*"));

	hFind = FindFirstFile(szDir, &ffd);

	do
	{
		if (ffd.cFileName[0] == '.')
			continue;
		if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{//folder
			TCHAR p[MAX_PATH];
			StringCchCopy(p, MAX_PATH, path);
			StringCchCat(p, MAX_PATH, TEXT("\\"));
			StringCchCat(p, MAX_PATH, ffd.cFileName);
			GetFileList(p);
		}
		else
		{//file
			TCHAR p[MAX_PATH];
			StringCchCopy(p, MAX_PATH, path);
			StringCchCat(p, MAX_PATH, TEXT("\\"));
			StringCchCat(p, MAX_PATH, ffd.cFileName);
			addTask(p);
			current_file.SetWindowTextW(ffd.cFileName);
		}
	} while (FindNextFile(hFind, &ffd) != 0);

	FindClose(hFind);
}

void CantivirDlg::addTask(LPTSTR p)
{
	while (true)
	{
		for (int i = 0; i < numCPU; i++)
		{
			if (s[i])
			{
				s[i] = false;//not free
				file[i] = p;
				return;
			}
		}
		Sleep(10);
	}
}